/*
 Navicat Premium Data Transfer

 Source Server         : TALLER1
 Source Server Type    : SQLite
 Source Server Version : 3021000
 Source Schema         : main

 Target Server Type    : SQLite
 Target Server Version : 3021000
 File Encoding         : 65001

 Date: 14/04/2020 21:33:34
*/

PRAGMA foreign_keys = false;

-- ----------------------------
-- Table structure for basket_player
-- ----------------------------
DROP TABLE IF EXISTS "basket_player";
CREATE TABLE "basket_player" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "name" varchar(150) NOT NULL,
  "nickname" varchar(80) NOT NULL,
  "birthday" date NOT NULL,
  "age" integer unsigned NOT NULL,
  "rut" varchar(12) NOT NULL,
  "email" varchar(254) NOT NULL,
  "height" integer unsigned NOT NULL,
  "weight" integer unsigned NOT NULL,
  "photo" varchar(100) NOT NULL,
  "position" varchar(2) NOT NULL,
  "team_id" integer NOT NULL,
  FOREIGN KEY ("team_id") REFERENCES "basket_team" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED,
   ("age" >= 0),
   ("height" >= 0),
   ("weight" >= 0)
);

-- ----------------------------
-- Records of basket_player
-- ----------------------------
INSERT INTO "basket_player" VALUES (1, 'Miguel Cantillana', 'mcantillana', '1984-11-15', 35, '15955611-5', 'mcantillana@portak53.cl', 185, 85, 'players/404Error.jpg', 'PI', 1);
INSERT INTO "basket_player" VALUES (2, 'Otro Jugador', 'otro', '2020-04-14', 11, '11111111-1', 'mcantillana@linets.cl', 176, 67, 'players/cafe_especialidad_chile_410x.png', 'ES', 1);

-- ----------------------------
-- Auto increment value for basket_player
-- ----------------------------
UPDATE "sqlite_sequence" SET seq = 2 WHERE name = 'basket_player';

-- ----------------------------
-- Indexes structure for table basket_player
-- ----------------------------
CREATE INDEX "basket_player_team_id_21e54eba"
ON "basket_player" (
  "team_id" ASC
);

PRAGMA foreign_keys = true;
