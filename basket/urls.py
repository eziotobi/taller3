from django.urls import path, include
from basket import views

app_name = 'basket'

urlpatterns = [
    path('players/list', views.players_list, name="players_list"),   
    path('players/create', views.player_create, name="players_create"),
    path('players/update/<int:pk>', views.player_update, name="players_update"),
    path('players/delete/<int:pk>', views.player_delete, name="players_delete"),
    path('teams/list', views.teams_list, name="teams_list"),
    path('teams/create', views.team_create, name="teams_create"),
    path('teams/update/<int:pk>', views.team_update, name="teams_update"),
    path('teams/delete/<int:pk>', views.team_delete, name="teams_delete"),
    path('coachs/list', views.coachs_list, name="coachs_list"),
    path('coachs/create', views.coach_create, name="coachs_create"),
    path('coachs/update/<int:pk>', views.coach_update, name="coachs_update"),
    path('coachs/delete/<int:pk>', views.coach_delete, name="coachs_delete"),
    path('matchs/list', views.matchs_list, name="matchs_list"),
    path('', views.home, name="home"),
]
