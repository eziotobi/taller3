from django import forms
from basket.models import Player, Team, Coach


class PlayerForm(forms.ModelForm):
    class Meta:
        model = Player
        fields = "__all__"


class TeamForm(forms.ModelForm):
    class Meta:
        model = Team
        fields = '__all__'


class CoachForm(forms.ModelForm):
    class Meta:
        model = Coach
        fields = '__all__'
