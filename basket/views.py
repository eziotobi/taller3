from django.shortcuts import render, redirect
from basket.models import (
    Team,
    Player,
    Coach,
    Match
)
from basket.forms import PlayerForm, TeamForm, CoachForm


def players_list(request):
    template_name = 'players_list.html'
    data = {}

    teams = Team.objects.all()

    for team in teams:
        team.players = Player.objects.filter(team=team)

    data['teams'] = teams

    return render(request, template_name, data)


def teams_list(request):
    template_name = 'teams_list.html'
    data = {}

    data['teams'] = Team.objects.all()
    return render(request, template_name, data)


def coachs_list(request):
    template_name = 'coach_list.html'
    data = {}
    data['coachs'] = Coach.objects.all()

    return render(request, template_name, data)


def matchs_list(request):
    template_name = 'matchs_list.html'
    data = {}

    data['matchs'] = Match.objects.all()

    return render(request, template_name, data)


def home(request):
    template_name = 'home.html'
    data = {}

    data['last_5_matchs'] = Match.objects.all().order_by('-pk')[:5]
    data['last_5_players'] = Player.objects.all().order_by('-pk')[:5]
    data['teams'] = Team.objects.all()

    return render(request, template_name, data)


def player_create(request):
    template_name = 'players_create.html'
    data = {}
    data['form'] = PlayerForm(request.POST or None)

    if data['form'].is_valid():
        data['form'].save()
        return redirect('basket:players_list')

    return render(request, template_name, data)


def team_create(request):
    template_name = 'team_create.html'
    data = {}
    data['form'] = TeamForm(request.POST or None)

    if data['form'].is_valid():
        data['form'].save()
        return redirect('basket:teams_list')

    return render(request, template_name, data)


def coach_create(request):
    print("aaaaaaaaaaaaaa")
    template_name = 'coach_create.html'
    data = {}
    data['form'] = CoachForm(request.POST or None)

    if data['form'].is_valid():
        print("coach saved")
        data['form'].save()
        return redirect('basket:coachs_list')

    else:
        print("not saved")

    return render(request, template_name, data)


def player_update(request, pk):
    template_name = 'players_update.html'
    data = {}

    player = Player.objects.get(pk=pk)
    data['form'] = PlayerForm(request.POST or None, instance=player)

    if data['form'].is_valid():
        data['form'].save()
        return redirect('basket:players_list')

    else:
        print("not saved")

    return render(request, template_name, data)


def coach_update(request, pk):
    template_name = 'coach_update.html'
    data = {}

    coach = Coach.objects.get(pk=pk)
    data['form'] = CoachForm(request.POST or None, instance=coach)

    if data['form'].is_valid():
        data['form'].save()
        return redirect('basket:coachs_list')

    else:
        print("not saved")

    return render(request, template_name, data)


def team_update(request, pk):
    template_name = 'team_update.html'
    data = {}

    team = Team.objects.get(pk=pk)
    data['form'] = TeamForm(request.POST or None, instance=team)

    if data['form'].is_valid():
        data['form'].save()
        return redirect('basket:teams_list')

    else:
        print("not saved")

    return render(request, template_name, data)


def player_delete(request, pk):
    player = Player.objects.get(pk=pk)
    player.delete()
    return redirect('basket:players_list')


def team_delete(request, pk):
    team = Team.objects.get(pk=pk)
    team.delete()
    return redirect('basket:teams_list')


def coach_delete(request, pk):
    coach = Coach.objects.get(pk=pk)
    coach.delete()
    return redirect('basket:coachs_list')
